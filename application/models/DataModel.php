<?php

class DataModel extends CI_Model
{
    public function insertExcel()
    {
        $dataExcel = json_decode($this->input->post('data'));
        $arrIns = array();
        for ($i = 0; $i < sizeof($dataExcel); $i++) {
            $objectInsert = array(
                'serialNumber' => $dataExcel[$i]->Serial_Number,
                'code' => $dataExcel[$i]->Voucher_Code,
                'refillId' => $dataExcel[$i]->Refill_Id,
                'tanggalUpload' => date('Y-m-d H:i:s'),
                'status' => '0',
            );
            array_push($arrIns, $objectInsert);
        }
        $q = $this->db->insert_batch('vouchergamegoogleplay', $arrIns);
        if ($q) {
            return array('success' => true, 'msg' => 'Successfully upload excel');
        } else {
            return array('success' => true, 'msg' => 'Failed to upload excel, Internal Server Error');
        }
    }

    public function insertVoucher()
    {
        $sn = $this->input->post('serialNumber');
        $vc = $this->input->post('voucherCode');
        $ri = $this->input->post('refillId');
        $cekSerial = $this->db->get_where('vouchergamegoogleplay', array('serialNumber' => $sn))->num_rows();
        if ($cekSerial < 1) {

            $array = array(
                'tanggalUpload' => date('Y-m-d H:i:s'),
                'status' => '0',
                'serialNumber' => $sn,
                'code' => $vc,
                'refillId' => $ri,
            );
            $q = $this->db->insert('vouchergamegoogleplay', $array);
            if ($q) {
                return array('success' => true, 'msg' => 'Successfully insert Voucher');
            } else {
                return array('success' => true, 'msg' => 'Failed to insert Voucher, Internal Server Error');
            }
        } else {
            return array('success' => false, 'msg' => 'Failed because the Serial Number is available on database, please change with different Serial Number');
        }
    }
    public function updateVoucher()
    {
        $sn = $this->input->post('serialNumber');
        $vc = $this->input->post('voucherCode');
        $ri = $this->input->post('refillId');
        $status = $this->input->post('statusVoucher');
        $this->db->where('serialNumber', $sn);
        $array = array(
            'tanggalUpload' => date('Y-m-d H:i:s'),
            'status' => $status,
            'code' => $vc,
            'refillId' => $ri,
        );
        $q = $this->db->update('vouchergamegoogleplay', $array);
        if ($q) {
            return array('success' => true, 'msg' => 'Successfully update Voucher');
        } else {
            return array('success' => true, 'msg' => 'Failed to update Voucher, Internal Server Error');
        }
    }

    public function insert_multiple($data)
    {
        // return
    }

}