<?php
 
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=".$filename.".xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
 ?>
 
 <table border="1" width="100%">
 
      <thead>
 
           <tr>
 
                <th>No.</th>
                <th>ID Channel</th>
                <th>ID Transaction</th>
                <th>Waktu</th>
                <th>Channel Name</th>
                <th>Master Number</th>
                <th>Ballance Now</th>
                <th>Begin Balance</th>
                <th>Transaction Balance</th>
                <th>To</th>
                <th>End Balance</th>
                <th>Note</th>
                <th>Type</th>
                <th>Mutation Type</th>
 
           </tr>
 
      </thead>
 
      <tbody>
 
           <?php $i=1; foreach($result as $r) {
           		$t = $r->type;
           		$type = "";
           		$mutType = "";
           		$mt = $r->mutation_type;
           		if ($t==1) {
           			$type = "Stock In";
           		}else if($t==2){
           			$type = "Stock Out";
           		}else{
           			$type = "Top Up";
           		}
				if ($mt==1) {
           			$mutType = "Parent to Child";
           		}else if ($mt==2) {
           			$mutType = "Child to Parent";
           		}else if ($mt==3) {
           			$mutType = "Child to Child";
           		}else if ($mt==4) {
           			$mutType = "Sibling";
           		}else if ($mt==5) {
           			$mutType = "Telkomcel to Dealer / Ballance Request";
           		}
            ?>
 		
           <tr>
 
                <td><?php echo $i; ?></td>
                <td><?php echo $r->idch; ?></td>
                <td><?php echo $r->idtrx; ?></td>
                <td><?php echo date('Ymd',strtotime($r->tx_date)); ?></td>
                <?php if ($t==1) {?>
                <td><?php echo $r->channel_name2; ?></td>
                <?php }else if($t==2){ ?>
                <td><?php echo $r->channel_name; ?></td>
                <?php }else{ ?>
                <td><?php echo $r->channel_name; ?></td>
                <?php } ?>
                <td><?php echo $r->master_number; ?></td>
                <td><?php echo $r->curentBallance; ?></td>
                <td><?php echo $r->begin_balance; ?></td>
                <td><?php echo $r->balanceTransaction; ?></td>
                <?php if ($t==1) {?>
                <td><?php echo $r->channel_name; ?></td>
                <?php }else if ($t==2){?>
                <td><?php echo $r->channel_name2; ?> </td>
                <?php }else{ ?>
                <td><?php echo $r->to; ?></td>
                <?php } ?>
                <td><?php echo $r->end_balance; ?></td>
                <td><?php echo $r->note; ?></td>
                <td><?php echo $type; ?></td>
                <td><?php echo $mutType; ?></td>
 
           </tr>
 
           <?php $i++; } ?>
 
      </tbody>
 
 </table>
