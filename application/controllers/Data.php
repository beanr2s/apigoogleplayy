<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Data extends CI_Controller
{

    public $filename = "kerjaan_baru";
    var $status = 0;

    public function __construct()
    {

        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        $this->load->model('DataModel', 'dm');
        $this->errorReturn = array('success' => false, 'data' => [], 'row' => 0);
    }

    public function getVoucherGooglePlay()
    {
        $sn = $this->input->get('sn');
        $denom = $this->input->get('denom');
        $stdate = $this->input->get('stDate');
        $enddate = $this->input->get('endDate');
        if ($sn) {
            $this->db->where('serialNumber', $sn);
        }
        if ($denom) {
            $this->db->where('refillId', $denom);
        }
        if ($stdate != "" and $enddate != "") {
            $this->db->where('DATE(tanggalPurchase) >=', date('Y-m-d', strtotime($stdate)));
            $this->db->where('DATE(tanggalPurchase) <=', date('Y-m-d', strtotime($enddate)));
        }
        $this->db->order_by('tanggalUpload', 'DESC');
        // $q = $this->db->get('vouchergamegoogleplay');
        $q =  $this->db->get_where('vouchergamegoogleplay', array('status' => $this->status));
        if ($q) {
            $result = array('success' => true, 'data' => $q->result());
        } else {
            $result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
        }
        $result['debugq'] = $this->db->last_query();
        echo json_encode($result);
    }

    public function saveForm()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            try {
                if ($this->input->post('serialNumberEdit')) {
                    $result = $this->dm->updateVoucher();
                } else {
                    $result = $this->dm->insertVoucher();
                }
            } catch (\Throwable $th) {
                $this->errorReturn['msg'] = $th->getMessage();
                $result = $this->errorReturn;
            }
            echo json_encode($result);
        }
    }

    public function saveExcel()
    {
        try {
            $result = $this->dm->insertExcel();
        } catch (\Throwable $th) {
            $result = array('success' => false, 'msg' => $th->getMessage());
        }
        echo json_encode($result);
    }

    public function deleteVoucher()
    {
        $sn = $this->input->post('sn');
        if ($sn != "") {
            $this->db->where('serialNumber', $sn);
            $q = $this->db->delete('vouchergamegoogleplay');
            if ($q) {
                $result = array('success' => true, 'msg' => 'Successfully delete Voucher');
            } else {
                $result = array('success' => true, 'msg' => 'Failed to delete Voucher, Internal Server Error');
            }
        } else {
            $result = array('success' => false, 'msg' => "Failed to delete because missing importants parameter");
        }
        echo json_encode($result);
    }

    public function importBack()
    {
        error_reporting(E_ALL ^ E_DEPRECATED);
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Upload
        $this->filename = $this->upload();

        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('uploads/googleplay/' . $this->filename); // Load file yang telah diupload ke folder excel
        $getSheet = $loadexcel->getSheetNames();
        foreach ($getSheet as $rows) {
            $sheet = $loadexcel->getSheetByName($rows)->toArray(null, true, true, true);
            // $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
            // var_dump($sheet); Buat sebuah variabel array untuk menampung array data yg
            // akan kita insert ke database
            $data = [];

            $numrow = 1;
            foreach ($sheet as $row) {
                // Cek $numrow apakah lebih dari 1 Artinya karena baris pertama adalah nama-nama
                // kolom Jadi dilewat saja, tidak usah diimport
                if ($numrow > 1) {
                    // Kita push (add) array data ke variabel data
                    $obj = array(
                        'serialNumber' => $this->cekSn($row['A'], $row['B'])['sn'],
                        // Serial Number
                        'tanggalUpload' => date('Y-m-d'),
                        'code' => $row['B'],
                        'refillId' => $rows,
                        'status' => 0,
                        'status_voucher' => $this->cekSn($row['A'], $row['B'])['status'],
                    );
                    array_push($data, $obj);
                }

                $numrow++; // Tambah 1 setiap kali looping
            }
            // var_dump($data);
            //  Panggil fungsi insert_multiple yg telah kita buat sebelumnya
            // di model
            try {
                $q = $this->db->insert_batch('vouchergamegoogleplay', $data);
                // $q = $this->dm->insert_multiple($data);
                if ($q) {
                    $log = array(
                        'success' => true,
                        'title' => 'Success',
                        'msg' => 'Berhasil import'
                    );
                    // redirect("http://150.242.111.235/googleplay/");
                } else {
                    $log = array(
                        'success' => false,
                        'title' => 'Failed',
                        'msg' => 'File Voucher already exists '
                    );
                    // redirect("http://150.242.111.235/googleplay?err=1");
                }
            } catch (Exception $th) {
                // echo $th->getMessage();
                $log = array(
                    'success' => false,
                    'msg' => 'Failed claused by ' . $th->getMessage()
                );
                // redirect('http://localhost/googleplay?err=1');
            }
        }
        // Redirect ke halaman awal (ke controller siswa fungsi index)
        echo json_encode($log);
    }
    // import data excel
    public function import()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Upload
        $this->filename = $this->upload();

        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('uploads/googleplay/' . $this->filename); // Load file yang telah diupload ke folder excel
        $getSheet = $loadexcel->getSheetNames();

        foreach ($getSheet as $rows) {
            $sheet = $loadexcel->getSheetByName($rows)->toArray(null, true, true, true);
            // $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
            // var_dump($sheet);
            // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
            $data = [];

            $numrow = 1;
            foreach ($sheet as $row) {
                // Cek $numrow apakah lebih dari 1
                // Artinya karena baris pertama adalah nama-nama kolom
                // Jadi dilewat saja, tidak usah diimport
                if ($numrow > 1) {
                    // Kita push (add) array data ke variabel data

                    array_push($data, array(

                        'serialNumber' => $this->cekSn($row['A'], $row['B'])['sn'], // Serial Number
                        'tanggalUpload' => date('Y-m-d'),
                        'code' => $row['B'],
                        'refillId' => $rows,
                        'status' => 0,
                        'status_voucher' => $this->cekSn($row['A'], $row['B'])['status'],
                        // Insert data alamat dari kolom D di excel
                    ));
                }

                $numrow++; // Tambah 1 setiap kali looping
            }

            // echo json_encode($data);
            var_dump($data);
            // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
            try {
                $q = $this->db->insert_batch('vouchergamegoogleplay', $data);
                // $q = $this->dm->insert_multiple($data);
                if ($q) {
                    $log = array('success' => true, 'title' => 'Success', 'msg' => 'Berhasil import');
                    // redirect("http://localhost/pencarian/");
                } else {
                    $log = array('success' => false, 'title' => 'Failed', 'msg' => 'File Voucher already exists ');

                    // redirect("http://localhost/pencarian?err=1");
                }
            } catch (Exception $th) {
                // echo $th->getMessage();
                $log = array('success' => false, 'msg' => 'Failed claused by ' . $th->getMessage());

                // redirect('http://localhost/googleplay?err=1');
            }
        }
        // Redirect ke halaman awal (ke controller siswa fungsi index)
        // $log = [
        //     'status' => true,
        //     'msg' => "Berhasil Import",
        // ];
        echo json_encode($log);
    }


    public function cekSn($sn = "", $voucher = "")
    {
        $log =  [];
        if ($sn == "") {

            $sub =  md5($voucher);
            $data = $sub;
            $log = [
                'sn' => $data,
                'status' => '0'
            ];
        } else {
            $data = $sn;
            $log = [
                'sn' => $data,
                'status' => '1'
            ];
        }

        return $log;
    }


    //upload excel
    public function upload()
    {
        $config['upload_path'] = "./uploads/googleplay";
        $config['allowed_types'] = 'xlsx|csv';
        $config['encrypt_name'] = false;

        $this->load->library('upload', $config);
        if ($this->upload->do_upload("file")) {
            $data = array('upload_data' => $this->upload->data());

            $image = $data['upload_data']['file_name'];
            return $image;
        }
    }
    public function getChartData()
    {
        $q = $this->db->query("SELECT COUNT(*) as counting, tanggalPurchase, refillId FROM vouchergamegoogleplay GROUP BY refillId, DATE(tanggalPurchase)");

        $series = array();
        $seriesData = array();
        $seriesName = "";
        foreach ($q->result() as $qr) {
            if ($qr->refillId != $seriesName) {
                $objSeries = array();
                $objSeries['name'] = $qr->refillId;
                $objSeries['data'] = array();
                array_push($series, $objSeries);
                var_dump($objSeries);
            }

            $obj = array();
            $obj['counting'] = $qr->counting;
            $obj['name'] = $qr->refillId;
            $obj['tgl'] = $qr->tanggalPurchase;
            array_push($seriesData, $obj);
            $seriesName = $qr->refillId;
        }
        echo json_encode(array('success' => true, 'series' => $series, 'dataSeries' => $seriesData));
    }
}
