<?php

defined('BASEPATH') or exit('No direct script access allowed');

class DataVoucherCdr extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        $this->load->model('ModelVoucher');
    }


    public function success()
    {
        $data = $this->ModelVoucher->success_response();
        echo json_encode($data);
    }


    //revenue Based 
    public function coba()
    {
        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');

        // baru start date and end date
        if ($startdate != "" and $enddate != "") {

            // $this->db->where('DATE(tanggalPurchase) >=', date('Y-m-d', strtotime($stdate)));
            // $this->db->where('DATE(tanggalPurchase) <=', date('Y-m-d', strtotime($enddate)));
            $success = $this->db->query("SELECT count(responseCode) as success_response, date, sum(amount) as total FROM cdr_vouchergame_detail where date between '$startdate' and '$enddate' and responseCode=0")->row();
            $success_nov = $this->db->query("SELECT count(responseCode) as norev_response, date, sum(amount) as total FROM cdr_vouchergame_detail where date between '$startdate' and '$enddate' and responseCode!=0 and responseCode!=500")->row();
            $error = $this->db->query("SELECT count(responseCode) as error_response, date, sum(amount) as total FROM cdr_vouchergame_detail where date between '$startdate' and '$enddate' and responseCode=500")->row();
        }


        $data = [
            [
                'name' => 'Success Rev',
                'y' => (int)$success->total,
                'sliced' => true,
                'selected' => true,

            ],
            [
                'name' => 'Success Non Rev',
                'y' => (int)$success_nov->total,
            ], [
                'name' => 'Error',
                'y' =>  $error->total == null ? 0 : (int)$error->total,
            ]
        ];
        // echo $success->total;
        echo json_encode($data);
        // echo json_encode($data);
    }
    public function detail()
    {

        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');

        // baru start date and end date

        $today = date('Y-m-d H:i:s');
        $data = [];
        $tanggal = [];
        $total = [];
        if ($startdate != "" and $enddate != "") {
            $query = $this->db->query("SELECT count(responseCode) as success_revenue, date, sum(amount) as total FROM cdr_vouchergame_detail where date between '$startdate' and '$enddate' and responseCode=0 group by date(date)")->result();
        }

        foreach ($query as $result) {
            array_push($tanggal, $result->date);
            array_push($total, (int)$result->total);
        }
        $data = [
            'tanggal' => $tanggal,
            'total' => $total,
        ];

        echo json_encode($data);
    }

    public function basicbar()
    {
        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');
        $data = [];
        $refillId = [];
        $total_pendapatan = [];
        if ($startdate != "" and $enddate != "") {
            $query = $this->db->query("SELECT refillId, count(*) as jlh_transaksi_sukses, date, sum(amount) as total_pendapatan FROM cdr_vouchergame_detail where date between '$startdate' and '$enddate' and responseCode=0 group by refillId order by total_pendapatan DESC")->result();
        }


        foreach ($query as $result) {
            array_push($refillId, $result->refillId);
            array_push($total_pendapatan, (int)$result->total_pendapatan);
        }
        $data = [
            'refillId' => $refillId,
            'total_pendapatan' => $total_pendapatan,
        ];

        echo json_encode($data);
    }


    //transaction TPS
    public function detail_tps()
    {

        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');
        $data = [];
        $tanggal = [];
        $total_pendapatan = [];

        if ($startdate != "" and $enddate != "") {
            $query = $this->db->query("SELECT refillId, count(*) as jlh_transaksi_sukses, date, sum(amount) as total_pendapatan FROM cdr_vouchergame_detail where date between '$startdate' and '$enddate' group by date order by date asc")->result();
        }

        foreach ($query as $result) {
            array_push($tanggal, $result->date);
            array_push($total_pendapatan, (int)$result->total_pendapatan);
        }
        $data = [
            'tanggal' => $tanggal,
            'total_pendapatan' => $total_pendapatan,
        ];

        echo json_encode($data);
    }


    public function basicbar_tiga()
    {
        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');
        $data = [];
        $refillId = [];
        $total_pendapatan = [];

        if ($startdate != "" and $enddate != "") {
            $query = $this->db->query("SELECT refillId, count(*) as jlh_transaksi_sukses, date, sum(amount) as total_pendapatan FROM cdr_vouchergame_detail  where date between '$startdate' and '$enddate' and responseCode=0  group by date order by date asc")->result();
        }
        //    
        foreach ($query as $result) {
            array_push($refillId, $result->refillId);
            array_push($total_pendapatan, (int)$result->total_pendapatan);
        }
        $data = [
            'refillId' => $refillId,
            'total_pendapatan' => $total_pendapatan,
        ];

        echo json_encode($data);
    }




    public function detail_success_rate()
    {

        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');
        $data = [];
        $tanggal = [];
        $total_pendapatan = [];

        if ($startdate != "" and $enddate != "") {
            $query = $this->db->query("SELECT refillId, count(*) as jlh_transaksi_sukses, date, sum(amount) as total_pendapatan FROM cdr_vouchergame_detail where date between '$startdate' and '$enddate' group by date order by date asc")->result();
        }



        foreach ($query as $result) {
            array_push($tanggal, $result->date);
            array_push($total_pendapatan, (int)$result->total_pendapatan);
        }
        $data = [
            'tanggal' => $tanggal,
            'total_pendapatan' => $total_pendapatan,
        ];

        echo json_encode($data);
    }
    //akhir success rate



    //detail dashboard gooogleplay

    public function getVoucherGooglePlayDetail()
    {


        $msisdn = $this->input->get('msisdn');
        $stdate = $this->input->get('stDate');
        $enddate = $this->input->get('endDate');

        if ($msisdn) {
            $this->db->where('msisdn', $msisdn);
        }
        if ($stdate != "" and $enddate != "") {
            $date = new DateTime($enddate);

            $date->modify('+1 day');
            $enddate_fix = $date->format('Y-m-d');
            $this->db->where('DATE(date) >=', date('Y-m-d', strtotime($stdate)));
            $this->db->where('DATE(date) <', $enddate_fix);

            // $q = $this->db->query("SELECT * FROM cdrvouchergame where date>='$stdate' and date <'$enddate_fix' order by date asc");
        }
        // else {
        //     $q = $this->db->query("SELECT * FROM cdrvouchergame order by  date asc");
        // }
        $this->db->order_by('date', 'ASC');
        $q = $this->db->get('cdrvouchergame');
        // $q = $this->db->get_where('vouchergamegoogleplay', array('status' => $this->status));
        if ($q) {
            $result = array('success' => true, 'data' => $q->result());
        } else {
            $result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
        }
        $result['debugq'] = $this->db->last_query();
        echo json_encode($result);
    }

    public function purchasePendingMobiwin()
    {

        // $this->db = $this->load->database('cdr', TRUE);


        $msisdn = $this->input->get('msisdn');
        $stdate = $this->input->get('stDate');
        $enddate = $this->input->get('endDate');

        if ($msisdn) {
            $this->db->where('msisdn', $msisdn);
        }
        if ($stdate != "" and $enddate != "") {
            $this->db->where('DATE(datetime) >=', date('Y-m-d', strtotime($stdate)));
            $this->db->where('DATE(datetime) <=', date('Y-m-d', strtotime($enddate)));
        }
        $this->db->order_by('datetime', 'ASC');
        //    $this->db->order_by('date', 'DESC');
        // $q =$this->db->get('cdr_vouchergame_detail');
        $q = $this->db->get('pendingtrx_gamesmobiwin');
        // $q = $this->db->get_where('vouchergamegoogleplay', array('status' => $this->status));
        if ($q) {
            $result = array('success' => true, 'data' => $q->result());
        } else {
            $result = array('success' => false, 'msg' => 'Failed to fetch all data Products Cat');
        }
        $result['debugq'] = $this->db->last_query();
        echo json_encode($result);
    }
}
/* End of file DataVoucherCdr.php */