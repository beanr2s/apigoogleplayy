<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Ussd extends CI_Controller {

    public function detail()
    {
        
        // $startdate = $this->input->get('startdate');
        // $enddate = $this->input->get('enddate');

        // baru start date and end date
        
        // $today = date('Y-m-d H:i:s');
        $data = [];
        $tanggal = [];
        $total = [];
        // if ($startdate != "" and $enddate != "") {
        $query = $this->db->query("SELECT count(responseCode) as success_revenue, date, sum(amount) as total FROM cdr_vouchergame_detail group by date(date)")->result();
      

        // }
        
        foreach ($query as $result) {
            array_push($tanggal, $result->date);
            array_push($total, (int)$result->total);
        }
        $data = [
            'tanggal' => $tanggal,
            'total' => $total,
        ];

        echo json_encode($data);
    }


    public function latihan()
    {
        $db2 =  $this->load->database('umb', TRUE);
         $time = [];
        $tanggal = [];
        $total = [];
        $query  = $db2->query("SELECT SUM(count) AS total, IF(counter = 'null', 'NULL', counter) AS counter, time FROM ussdnode72 GROUP BY DATE(time) , counter")->result();
        
        foreach ($query as $result) {
            array_push($tanggal, $result->time);
            array_push($total, (int)$result->total);
        }
        $data = [
            'tanggal' => $tanggal,
            'total' => $total,
        ];

        echo json_encode($data);
    }

}

/* End of file Ussd.php */
